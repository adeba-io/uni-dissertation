extends Node

export(NodePath) var _np_collision_shape: NodePath
export(NodePath) var _np_panel: NodePath

onready var _coll_shape: CollisionShape2D = get_node(_np_collision_shape)
onready var _panel: Control = get_node(_np_panel)

func _ready() -> void:
	var shape := _coll_shape.shape
	
	var extents := _panel.rect_size * 0.5
	shape.extents = extents
	
	var center := _panel.rect_position + extents
#	print(center)
	(_coll_shape.get_parent() as Node2D).position = center
#	print(extents)
