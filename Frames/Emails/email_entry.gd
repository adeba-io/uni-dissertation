extends Control
class_name EmailEntry

signal forward_gui_input(event)

export var _email_open_step := 0.0

onready var sender: Label = $Sender
onready var subject: Label = $Subject
onready var body: Label = $Body

onready var _anim_player: AnimationPlayer = $AnimationPlayer
onready var _timer: Timer = $Timer

var _is_expanded := false
var _is_animating := false

var _collapsed_height: float
var _max_height: float

var _collapsed_body_height: float
var _expanded_body_height: float
var _collapsed_body_y: float
var _expanded_body_y: float

const HOLD_TIMEOUT := 0.2
const MOTION_EVENT_THRESHOLD := 6
var _motions_since_hold := 0
var _under_hold_timeout := true
var _forward_events := false

func _ready() -> void:
	_collapsed_body_height = body.rect_size.y
	_collapsed_body_y = body.rect_position.y
	var diff := body.rect_size.y - body.get_line_height()
	body.autowrap = true
	_expanded_body_height = body.get_line_count() * body.get_line_height() + diff
	body.autowrap = true
	
	_expanded_body_y = _collapsed_body_y #- (_expanded_body_height / 2)
	
	_collapsed_height = rect_size.y
	rect_min_size.y = rect_size.y
	_max_height = body.rect_position.y + _expanded_body_height + 10

func _on_gui_input(event: InputEvent) -> void:
	var is_mouse_button_event: bool = event is InputEventMouseButton and event.button_index == BUTTON_LEFT
	
	if _forward_events:
		emit_signal("forward_gui_input", event)
#		if is_mouse_button_event and not event.pressed:
#			_forward_events = false
		# Stop forwarding events when we get a mouse button not pressed event
		_forward_events = not (is_mouse_button_event and not event.pressed)
		return
	
	if not is_mouse_button_event:
		return
	
	if event.pressed:
		_begin_hold_timeout()
		return
	
#	print("ayy")
	_under_hold_timeout = false
	_forward_events = false
	_is_expanded = not _is_expanded
	_is_animating = true
	_anim_player.play("open" if _is_expanded else "close")
	
	while _is_animating:
		rect_min_size.y = lerp(_collapsed_height, _max_height, _email_open_step)
		body.rect_position.y = lerp(_collapsed_body_y, _expanded_body_y, _email_open_step)
		body.rect_size.y = lerp(_collapsed_body_height, _expanded_body_height, _email_open_step)
		yield(get_tree(), "idle_frame")

func _on_animation_finished(_anim_name: String) -> void:
	_is_animating = false

func _begin_hold_timeout() -> void:
#	print("begin")
	_under_hold_timeout = true
	_timer.start(HOLD_TIMEOUT)
	_motions_since_hold = 0

func _on_timeout():
	if not _under_hold_timeout:
		return
	
#	print("timeout")
	_under_hold_timeout = false
	_forward_events = true
	var event := InputEventMouseButton.new()
	event.pressed = true
	event.button_index = BUTTON_LEFT
	emit_signal("forward_gui_input", event)
