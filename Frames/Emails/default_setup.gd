extends Node

const SCN_EMAIL: PackedScene = preload("res://Frames/Emails/email_entry.tscn")
const SCN_BREAK: PackedScene = preload("res://Frames/Emails/email_date_break.tscn")
onready var _frame: Frame = get_parent()

func _ready():
	var data := EmailGenerator.generate_emails(10, Database.usernames, "Website")
	
	var parent: EmailView = get_parent()
	parent.all_emails = data
	var entries := EmailGenerator.populate_email_view(data, parent, SCN_EMAIL, SCN_BREAK)
	for entry in entries:
		entry.connect("forward_gui_input", self, "_forward_entry_input_event")
		
	get_node("../Node").assign_frames([ _frame ])


func _forward_entry_input_event(event: InputEvent) -> void:
	_frame._on_StaticBody2D_input_event(null, event, 0)
