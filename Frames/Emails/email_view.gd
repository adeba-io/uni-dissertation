extends Frame
class_name EmailView

const SCN_ENTRY: PackedScene = preload("res://Frames/Emails/email_entry.tscn")
const SCN_BREAK: PackedScene = preload("res://Frames/Emails/email_date_break.tscn")

onready var _container: Node = Helper.get_descendents_in_group(self, [ 'entry_container' ]).front()

var all_emails: Dictionary

# days_back -> [ KGEmail ]
func assign_emails(website: KGWebsite, emails: Dictionary) -> void:
	var days := emails.keys()
	days.sort()
	
	for day in days:
		if day != 0:
			var brk := SCN_BREAK.instance()
			var label: Label = Helper.get_children_of_type(brk, 'Label').front()
			label.text = label.text % day
			_container.add_child(brk)
#			brk.connect("forward_gui_input", self, "_forward_entry_input_event")
		
		for email in emails[day]:
			var entry: EmailEntry = SCN_ENTRY.instance()
			_container.add_child(entry)
			var from: String
			if sign(email.from) < 0:
				from = website.personnel[-email.from - 1].forename
			else:
				from = website.users[email.from].username 
			entry.sender.text = from
			entry.subject.text = email.subject
			entry.body.text = email.body
			
			entry.connect("forward_gui_input", self, "_forward_entry_input_event")
	
func _forward_entry_input_event(event: InputEvent) -> void:
	_on_StaticBody2D_input_event(null, event, 0)
