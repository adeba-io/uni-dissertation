extends ScrollContainer

export(float, 1.0, 5.0) var scroll_factor := 2.0

var _scroll: VScrollBar

func _ready() -> void:
	for child in get_children():
		if child.get_class() == "VScrollBar":
			_scroll = child
			break

#func _on_StaticBody2D_input_event(viewport, event, shape_idx):
#	if event is InputEventMouseButton:
#		match event.button_index:
#			BUTTON_WHEEL_UP:
#				print(_scroll.ratio)
#			BUTTON_WHEEL_DOWN:
#				print(_scroll.ratio)


func _on_StaticBody2D_mouse_entered():
	pass

func _on_StaticBody2D_mouse_exited():
	pass # Replace with function body.
