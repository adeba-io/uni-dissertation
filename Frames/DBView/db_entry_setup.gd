extends Node

func _on_headers_assigned(headers: Array) -> void:
	var templates: Node = $"../ColTemplates"
	var data_parent: Node    = $"../Data"
	
	var header_groups := Helper.get_by_keys(DBVGenerator.TYPE_GROUP, headers)
	var labels := {}
	for label in Helper.get_children_of_type(templates, 'Label'):
		if not Helper.has_any(label.get_groups(), header_groups):
			continue
		
		labels[label.get_groups()[0]] = label
	
	var offset := 0.0
	var labels_in_use := {}
	for header in headers:
		var name: String = DBVGenerator.TYPE_GROUP[header]
		var label: Label = labels[name].duplicate()
		data_parent.add_child(label)
		
		var mr := label.margin_right
		label.margin_left += offset
		label.margin_right += offset
		offset += mr
		label.visible = true
		labels_in_use[name] = { lbl = label, compliant = true }
	
	var parent: DBEntry = get_parent()
	parent.width = offset + 20
	parent._labels = labels_in_use
