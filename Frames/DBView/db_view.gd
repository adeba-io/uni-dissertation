extends Frame
class_name DBView

onready var _container: Node = Helper.get_descendents_in_group(self, [ 'dbv_container' ]).front()

# [ [ { data, width, is_faulty, display_type } ] ]
func setup(all_data: Array, entry_scene: PackedScene, display_faults := false) -> void:
	for data in all_data:
		var entry := entry_scene.instance()
		_container.add_child(entry)
		entry.assign_data(data, display_faults)
	
	var width := 30.0
	for entry in all_data[0]:
		width += entry.width
	
	set_frame_width(width)
