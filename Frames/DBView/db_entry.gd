extends Control
class_name DBEntry

enum DisplayType { FADE, CUTOFF }

export var good_text_color := Color.white
export var bad_text_color  := Color.red

onready var _field_parent := $Data
onready var _labels: Dictionary = { DisplayType.FADE: $ColTemplates/Fade, DisplayType.CUTOFF: $ColTemplates/Cutoff }

var _faulty_fields := []

# [ { data, width, is_faulty, display_type } ]
func assign_data(data: Array, display_fault := false) -> void:
	
	var x := 0.0
	for field in data:
		var label: Label = _labels[field.display_type].duplicate()
		label.margin_left = x
		x += field.width
		label.margin_right = x
		x += 5.0
		
		label.text = field.data
		if field.is_faulty:
			_faulty_fields.append(label)
		
		label.add_color_override("font_color", 
		   bad_text_color if display_fault and field.is_faulty else good_text_color)
		_field_parent.add_child(label)
		label.visible = true

func display_faults(show: bool) -> void:
	for field in _faulty_fields:
		field.add_color_override("font_color", bad_text_color if show else good_text_color)
