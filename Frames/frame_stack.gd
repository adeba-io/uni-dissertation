extends Node
class_name FrameStack

var _frame_stack := []
var _frames_clicked_this_frame := []

func assign_frames(frames: Array) -> void:
	for frame in _frame_stack:
		frame.disconnect("clicked", self, "_on_frame_clicked")
		frame.disconnect("frame_tree_exiting", self, "_on_frame_tree_exiting")
	
	_frame_stack = frames
	for frame in _frame_stack:
		frame.connect("clicked", self, "_on_frame_clicked")
		frame.connect("frame_tree_exiting", self, "_on_frame_tree_exiting")

func _frame_to_top(frame: Frame) -> void:
	_frame_stack.erase(frame)
	_frame_stack.append(frame)

func _process(_delta: float) -> void:
	if _frames_clicked_this_frame.size() != 0:
		var frame: Frame = _frames_clicked_this_frame.back()
		frame.captured = true
		_frame_to_top(frame)
		_set_indexes()
		_frames_clicked_this_frame.clear()

func _on_frame_clicked(frame: Frame) -> void:
	if not frame in _frames_clicked_this_frame:
		_frames_clicked_this_frame.append(frame)
 
func _set_indexes() -> void:
	for i in range(_frame_stack.size()):
		_frame_stack[i].z_index = i

func _on_frame_tree_exiting(frame: Frame) -> void:
	_frame_stack.erase(frame)
