tool
extends Frame
class_name FrPreview

export var preview_image: Texture setget set_image

onready var _texture: TextureRect = $UI/Content/TextureRect

func set_image(img: Texture) -> void:
	_texture.rect_size = img.get_size()
	_texture.texture = img
	if not Engine.editor_hint:
		._resize_collision()
