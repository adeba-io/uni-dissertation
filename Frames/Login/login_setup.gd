extends Node

const SCN_VALID := preload("res://Frames/Login/correct_login.tscn")

const SCN_NO_GDPR := preload("res://Frames/Login/login_no_gdpr.tscn")

const BAD_LOGIN_CHANCE := 0.34

func _ready() -> void:
#	if not GDPRViolations.e.CLEAR_REQUEST_FOR_CONSENT in RoundConfig.enabled_violations:
#		get_parent().queue_free()
#		return
	
	var DOM := get_node("../UI/Content/DOM")
	
	var rng := RandomNumberGenerator.new()
	rng.randomize()
	
	var page: PackedScene
	var violations := []
	var is_compliant := true
	if rng.randf() < BAD_LOGIN_CHANCE:
		page = SCN_NO_GDPR
		violations.append(GDPRViolations.e.CLEAR_REQUEST_FOR_CONSENT)
		is_compliant = false
	else:
		page = SCN_VALID
	
	DOM.add_child(page.instance())
	var parent: Frame = get_parent()
	parent.violations = violations
	parent.is_compliant = is_compliant
