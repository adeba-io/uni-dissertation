extends Frame
class_name LoginDemo

enum Type { VALID, OBSCURE, NO_CONSENT }

const SCENES := { 
	Type.VALID: preload("res://Frames/Login/correct_login.tscn"),
	Type.NO_CONSENT: preload("res://Frames/Login/login_no_gdpr.tscn")
}

onready var DOM := $UI/Content/DOM

func setup(type: int) -> void:
	var page: Node = SCENES[type].instance()
	
	if type != Type.VALID:
		violations = [ GDPRViolations.e.CLEAR_REQUEST_FOR_CONSENT ]
