extends Node2D
class_name Frame

signal clicked(node)
signal frame_tree_exiting(frame)

export(float, 1.0, 1.5) var _corrective_factor := 1.2

onready var _coll_shape: CollisionShape2D = $StaticBody2D/CollisionShape2D
onready var _panel: Control = $UI

var captured := false
var mouse_pos := Vector2()

var is_compliant := true
var violations := []

var _width := -1.0

func set_frame_width(width: float) -> void:
	if _panel:
		_panel.rect_size.x = width
		_resize_collision()
	else:
		_width = width

func _ready() -> void:
	_coll_shape.shape = RectangleShape2D.new()
	
	if _width > 0.0:
		_panel.rect_size.x = _width
	_resize_collision()

func _resize_collision() -> void:
	var extents := _panel.rect_size * 0.5
	_coll_shape.shape.extents = extents
	var center := _panel.rect_position + extents
	(_coll_shape.get_parent() as Node2D).position = center
	
func _on_StaticBody2D_input_event(_viewport, event, _shape_idx) -> void:
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
#		print('button')
		if event.pressed:
			get_tree().set_input_as_handled()
			emit_signal("clicked", self)
			mouse_pos = get_global_mouse_position()
		else:
			captured = false
	
	elif event is InputEventMouseMotion:
#		print('motion')
		if captured:
			position += (get_global_mouse_position() - mouse_pos) * _corrective_factor
			mouse_pos = get_global_mouse_position() 
			get_tree().set_input_as_handled()

func _on_StaticBody2D_mouse_exited():
	captured = false

func _exit_tree() -> void:
	emit_signal("frame_tree_exiting", self)
