# Game Flow Document

## GDPR Rules to Cover

## Priority

1 - transparent processing  

2 - collected for specific, explicit and legimate purposes + not further processed beyond these  

3 - steps must be taken to ensure that data is accurate  

4 - kept in a form which permits identification of data subjects  

5 - not stored for longer than necessary  

6 - the controller shall be able to demonstrate the subject has consented  

7 - the data subject has the right to withdraw consent at any time  

8 - request for consent must be clear and distinct from other permissions  

9 - if the subject is below 16 years of age the controller must ensure that the user has parental consent

10 - the data subject has the right to confirm from the controller whether or not their data is being processed

11 - purposes, categories of data, recipients of the processed data

12 - how long the data will be stored/the criteria for determining how long

13 - the controller shall provide a copy of the data being processed, so long as it does not infringe on the rights and freedoms of others

14 - the data subject shall have the right to obtain the rectification of inaccurate personal data

15 - data subject has the right to be forgotten

16 - unless: it infringes on freedom of information, for the establishment or defence of legal claims

17- archiving in the public interest

18- in the case of a data breach, the controller shall within 72 hours of becoming aware notify the breach to supervisory body, or provide reasons for the delay

19 - the controller shall also communicate this breach to the data subjects

## Game Flow

You are presented with a set of websites, that may or may not be in compliance with GDPR.  
GDPR compliance will be judged through inspecting the the website's databases, their login/account creation
process and the any emails they have both internally and with their users.  
To give context to the the data, each site is accompanied with a banner and some marketing material.  
All of this data will be expressed as moveable 'frames'.

The sites themselves will be represented by small icons that travel along a path toward a 'bin'.
One of these bins will be used to mark the website as compliant. The other bins will be used to
mark that the website is not compliant and where it's violation is.

The general objective is for the player to go through as many of these websites - 
marking as either compliant or non-compliant - within a time limit.

In earlier rounds, websites will only showcase a few rules, have one violation and 
the stream of incoming websites will be slow.
In later rounds this will the number of rules tested and the number of violations will increase,
and the websites will travel and be more frequent.

## Rule Implementations

#### 2

Showcase the purpose of the website. Then showcase parts of the database that may opppose it.
DBViews

#### 5

Time limits, after X many years
DB Views

### 6

DBView showing consent + view to check if it the consent is being followed
Click on a user to get all their info - **a user profile frame**

#### 7

Consent withdrawal emails + checking if they are in the DB

#### 8

Login preview

#### 10, 11, 12

Emails

#### 18

Search a gov databse to see if they've gotten the notification

#### 19

Search emails


## Frame Types

Database View:
Just a look into the database, a spreadsheet, boring
Need different ways to represent it, that isn't just a skin

Email Inbox:
A collective inbox of emails, user request for data, queries about reitification

Login/Sign Up preview
A demo of the sign up process for the website.
Used to show that the website does ask for consent

User Profile
A collection of the information on a single user
