 class_name DtGenerator

const YR_BASELINE := 2010

static func dates(min_my := 0, mapper := FuncRef.new()) -> Dictionary:
	# We'll have Jan 2010 as the baseline
	var date := OS.get_date(true)
	var max_month_year: int = (date['year'] - YR_BASELINE) * 12 + date['month']
	
	if min_my >= max_month_year:
		return { "composite": max_month_year, "str": "%02d/%d" % [ date['month'], date['year'] ] }
	
	var rng := RandomNumberGenerator.new()
	rng.randomize()
	var r := rng.randf()
	var my: int = lerp(min_my, max_month_year, mapper.call_func(r) if mapper.is_valid() else r)
	
#	var my := rng.randi_range(min_my, max_month_year)
	var month := my % 12 + 1
# warning-ignore:integer_division
	var year := my / 12 + YR_BASELINE
	
	return { "composite": my, "str": "%02d/%d" % [ month, year ]   }

static func get_max_dates() -> Dictionary:
	var date := OS.get_date(true)
	var max_month_year: int = (date['year'] - YR_BASELINE) * 12 + date['month']
	return { "composite": max_month_year, "str": "%02d/%d" % [ date['month'], date['year'] ] }

static func hashed_password(length: int) -> String:
	var rng := RandomNumberGenerator.new()
	rng.randomize()
	var s := ''
	for _i in range(length):
		s += char(rng.randi_range(33, 126))
	return s
