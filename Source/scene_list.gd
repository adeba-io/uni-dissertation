extends Resource
class_name SceneList

export(Array, PackedScene) var scenes := []
