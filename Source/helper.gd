class_name Helper

static func get_siblings(node: Node) -> Array:
	var siblings := node.get_parent().get_children()
	siblings.erase(node)
	return siblings

static func get_descenedents(node: Node) -> Array:
	var children := []
	for child in node.get_children():
		children.append(child)
		children.append_array(get_descenedents(child))
	return children

static func get_descendents_in_group(node: Node, groups: Array) -> Array:
	var children := []
	for child in node.get_children():
		for group in groups:
			if child.is_in_group(group):
				children.append(child)
				break
		
		children.append_array(get_descendents_in_group(child, groups))
	return children

static func get_world_pos(control: Control) -> Vector2:
	var vp := control.get_viewport()
	return vp.get_canvas_transform().affine_inverse().xform(control.rect_global_position)

static func random_array(arr: Array) -> Array:
	var rng := RandomNumberGenerator.new()
	rng.randomize()
	var d := {}
	for i in range(arr.size()):
		var n := rng.randi()
		d[n] = i
	
	var keys := d.keys()
	keys.sort()
	
	var newArr := []
	for k in keys:
		newArr.append(arr[d[k]])
	
	return newArr

static func get_random_from_array(arr: Array):
	var rng := RandomNumberGenerator.new()
	rng.randomize()
	return arr[rng.randi_range(0, arr.size() - 1)]

static func move_child(child: Node, new_parent: Node) -> void:
	var old_parent := child.get_parent()
	old_parent.remove_child(child)
	new_parent.add_child(child)

static func get_children_of_type(node: Node, classs: String) -> Array:
	var children: = []
	for child in node.get_children():
		if child.is_class(classs):
			children.append(child)
		children.append_array(get_children_of_type(child, classs))
	return children

static func get_of_group(nodes: Array, groups: Array) -> Array:
	var in_group := []
	for node in nodes:
		for group in groups:
			if node.is_in_group(group):
				in_group.append(node)
				break
	return in_group

static func free_children(node: Node) -> void:
	for child in node.get_children():
		child.queue_free()

static func get_by_keys(dict: Dictionary, keys: Array) -> Array:
	var arr := []
	for key in keys:
		if not key in dict:
			continue
		
		arr.append(dict[key])
	return arr

static func has_any(arr: Array, checks: Array) -> bool:
	for check in checks:
		if arr.has(check):
			return true
	return false

static func filter(arr: Array, to_include: FuncRef) -> Array:
	if not to_include.is_valid():
		return arr.duplicate(true)
	
	var new_arr := []
	for x in arr:
		if to_include.call_func(x):
			new_arr.append(x)
	return new_arr

static func filter_by_group(nodes: Array, group: String) -> Array:
	var arr := []
	for node in nodes:
		if node.is_in_group(group):
			arr.append(node)
	return arr

static func uniques_only(array: Array) -> Array:
	var arr := []
	for x in array:
		if not x in arr:
			arr.append(x)
	return arr

static func random_from_array(array: Array, rng: RandomNumberGenerator):
	return array[rng.randi_range(0, array.size() - 1)]

static func all_elements_arr_in_other(arr: Array, other: Array) -> bool:
	for x in arr:
		if not x in other:
			return false
	return true

static func intesect(array1: Array, array2: Array) -> Array:
	var arr := []
	for x in array1:
		if x in array2:
			arr.append(x)
	return arr

static func intersect_dict(array: Array, dict: Dictionary) -> Dictionary:
	var d := {}
	for x in array:
		if x in dict:
			d[x] = dict[x]
	return d
