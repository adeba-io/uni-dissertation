extends Label

export(NodePath) var _path_range: NodePath

onready var _range: Range = get_node(_path_range)

func _ready() -> void:
	_range.connect('value_changed', self, "_on_value_changed")
	text = str(int(_range.value))

func _on_value_changed(value: float) -> void:
	text = str(int(value))
