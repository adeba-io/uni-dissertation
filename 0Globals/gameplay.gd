extends Node

const game_scene: PackedScene = preload("res://Stage/stg_default.tscn")
onready var websites: SceneList = load('res://Site/site_catalogue.tres')

var rng := RandomNumberGenerator.new()

var stage: Node
var curr_website: Website
var curr_icon: Control

var successes := 0
var misses := 0

var banner_parent: Control
var btn_accept: Control
var btn_reject: Control

var countdown: Countdown
var results_screen: ResultsScreen

var cnt_site_icon: Control

func begin_round(level_num: String) -> void:
	rng.randomize()
	var err := get_tree().change_scene_to(game_scene)
	if err != OK:
		print("Error with changing to game scene\n\t" + str(err))
	yield(get_tree(), "idle_frame")
	
	stage = get_tree().get_nodes_in_group("stage").front()
	
	successes = 0
	misses = 0
	
	cnt_site_icon = get_tree().get_nodes_in_group("cnt_site_icon").front()
	banner_parent = get_tree().get_nodes_in_group("banner").front()
	results_screen = get_tree().get_nodes_in_group("res_screen").front()
	
	btn_accept = get_tree().get_nodes_in_group("btn_accept").front()
# warning-ignore:return_value_discarded
	btn_accept.connect("button_down", self, "_on_accept")
	btn_reject = get_tree().get_nodes_in_group("btn_reject").front()
# warning-ignore:return_value_discarded
	btn_reject.connect("button_down", self, "_on_reject")
	
	countdown = get_tree().get_nodes_in_group("countdown").front()
	countdown.start_countdown(RoundConfig.round_duration)
	countdown.connect_to_timeout(self, "_on_countdown_timeout")
	
	get_tree().get_nodes_in_group('level_label').front().text = 'Level %s' % level_num
	
	_update_current_site()

func _update_current_site() -> void:
	if curr_website:
		print(curr_icon)
		cnt_site_icon.remove_child(curr_icon)
		curr_icon.queue_free()
		curr_website.queue_free()
		curr_website  = null
		curr_icon = null
	
	# NOTE _ready is not called until it is added to the scene tree
	
	curr_website = websites.scenes[rng.randi_range(0, websites.scenes.size() - 1)].instance()
	stage.add_child(curr_website)
	
	curr_icon = curr_website.icon
	curr_icon.disabled = false
	Helper.move_child(curr_icon, cnt_site_icon)
	
	var banner: Control = curr_website.banner
	if banner:
		Helper.move_child(banner, banner_parent)

func fn_accept_reject(accept: bool) -> void:
	if not curr_website:
		return

	if accept == curr_website.is_compliant:
		successes += 1
	else:
		misses += 1

	_update_current_site()

func _on_accept() -> void:
	fn_accept_reject(true)

func _on_reject() -> void:
	fn_accept_reject(false)
	
func end_level() -> void:
	countdown.stop()
	_on_countdown_timeout()

func _on_countdown_timeout() -> void:
	if curr_website:
		curr_icon.disabled = true
		curr_website = null

	print("Round complete")
	results_screen.start({ "successes": successes, "misses": misses  })
