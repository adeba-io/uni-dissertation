class_name GDPRViolations

enum e {
	SPECIFIC_EXPLICIT_LEGIT,
	ENSURE_ACCURACY,
	IDENTIFICATION_OF_SUBJECTS,
	NOT_STORED_FOR_LONGER_THAN_NECESSARY,
	DEMONSTRATE_CONSENT,
	RIGHT_TO_WITHDRAW_CONSENT,
	CLEAR_REQUEST_FOR_CONSENT,
	PROPER_PARENTAL_CONSENT,
	RIGHT_TO_CONFIRM_DATA_IS_BEING_PROCESSED,
#	PURPOSES_CATEGORIES_RECIPIENTS_OF_PROCESSED_DATA,
	LENGTH_OF_TIME_OF_DATA_STORAGE,
	PROVIDE_COPY_OF_DATA_BEING_PROCESSED,
	RIGHT_TO_OBTAIN_RECTIFICATION,
	RIGHT_TO_BE_FORGOTTEN,
	DATA_BREACH_INFORM_SUPERVISORY_BODY,
	DATA_BREACH_INFORM_SUBJECTS
}

static func get_violation_strings(types: Array) -> Array:
	var strings := []
	for type in types:
		strings.append(get_full_string(type))
	return strings

static func get_full_string(type: int) -> String:
	match type:
		e.SPECIFIC_EXPLICIT_LEGIT:
			return "Collected for specific, explicit and legitimate purposes"
		e.ENSURE_ACCURACY:
			return "Steps must be taken to ensure that data is accurate"
		e.IDENTIFICATION_OF_SUBJECTS:
			return "Kept in a form which permits identification of data subjects"
		e.NOT_STORED_FOR_LONGER_THAN_NECESSARY:
			return "Not stored for longer than necessary"
		e.DEMONSTRATE_CONSENT:
			return "The controller shall be able to demonstrate that the subject has consented"
		e.RIGHT_TO_WITHDRAW_CONSENT:
			return "The data subject has the right to withdraw consent at anytime"
		e.CLEAR_REQUEST_FOR_CONSENT:
			return "The request for consent must be clear and distinct from other permissions"
		e.PROPER_PARENTAL_CONSENT:
			return "If the subject is below 16 years old, the controller must ensure the user has parental consent"
		e.RIGHT_TO_CONFIRM_DATA_IS_BEING_PROCESSED:
			return "The data subject has the right to confirm from the controller whether or not their data is being processed"
#		e.PURPOSES_CATEGORIES_RECIPIENTS_OF_PROCESSED_DATA:
#			return ""
		e.LENGTH_OF_TIME_OF_DATA_STORAGE:
			return "How long will the data be stored and what's the criterea for determining this"
		e.PROVIDE_COPY_OF_DATA_BEING_PROCESSED:
			return "The controller shall provide a copy of the data processed, unless it infringes on the rights of othhers"
		e.RIGHT_TO_OBTAIN_RECTIFICATION:
			return "The data subject has the right to obtain rectification of inaccurate personal data"
		e.RIGHT_TO_BE_FORGOTTEN:
			return "The data subject has the right to be forgotten unless it infringes on the freedom of information"
		e.DATA_BREACH_INFORM_SUPERVISORY_BODY:
			return "In the case of data breach, the controller shall inform the supervisory body within 72 hours of becoming aware of it"
		e.DATA_BREACH_INFORM_SUBJECTS:
			return "In the case of data breach, the controller shall inform the data subjects affected"
	return "Unknown"
