extends Node
"""
This needs to be redone so that instead of being a singleton it is
a node that is a child to the Website
The Frames should comunicate with it via signals
"""


var node_just_pressed := false
var _stack := []
var _clicked_this_frame := []

func add(node: Frame):
	_stack.append(node)

#func remove(node: Frame):
#	_stack.erase(node)

func clear() -> void:
	_stack.clear()

func clicked(node: Frame):
	if not node in _clicked_this_frame:
		_clicked_this_frame.append(node)

func window_to_top(node: Frame):
	var i = _stack.find(node)
	if i == -1:
		_stack.append(node)
	else:
		_stack.remove(i)
		_stack.append(node)

func _process(_delta: float):
#	if node_just_pressed:
#		node_just_pressed = false
#		_set_indexes()
	if len(_clicked_this_frame) == 1:
		_clicked_this_frame[0].captured = true
		window_to_top(_clicked_this_frame[0])
		_set_indexes()
		_clicked_this_frame.clear()
	elif len(_clicked_this_frame) > 1:
		var largest: Frame = _clicked_this_frame[0]
		for i in range(1, len(_clicked_this_frame)):
			if largest.z_index < _clicked_this_frame[i].z_index:
				largest = _clicked_this_frame[i]
		
		largest.captured = true
		window_to_top(largest)
		_set_indexes()
		_clicked_this_frame.clear()
	

func _set_indexes():
	for i in range(len(_stack)):
		_stack[i].z_index = i
