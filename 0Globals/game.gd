extends Node

const MAIN_MENU := preload('res://Menu/main_menu.tscn')

# ###################
# LEVELS

const LEVELS := [
	preload('res://Levels/level1.tres'),
	preload('res://Levels/level2.tres'),
	preload('res://Levels/level3.tres'),
	preload('res://Levels/level4.tres'),
	preload('res://Levels/level5.tres')
]

var current_level := -1
var free_mode_unlocked :=  false


func next_level() -> void:
	if free_mode_unlocked:
		var err := get_tree().change_scene_to(MAIN_MENU)
		if err != OK:
			print("Next Button has received an errror with changing scenes\n\t" + str(err))
		return
		
	
	current_level += 1
	
	if current_level >= LEVELS.size():
		free_mode_unlocked = true
		var err := get_tree().change_scene_to(MAIN_MENU)
		if err != OK:
			print("Next Button has received an errror with changing scenes\n\t" + str(err))
		return
	
	var settings: LevelSettings = LEVELS[current_level]
	RoundConfig.round_duration = settings.time
	RoundConfig.enabled_violations = settings.violations
#	RoundConfig.show_faults = true
	RoundConfig.user_count = settings.user_count
	MngGameplay.begin_round(str(current_level + 1))

func unlock_free_mode() -> void:
	current_level = LEVELS.size()
	free_mode_unlocked = true
