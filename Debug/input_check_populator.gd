extends Control

const SCRIPT := preload("res://Debug/input_check.gd")

func _ready() -> void:
	yield(get_tree(), "idle_frame")
	var nodes := [ self ]
	nodes.append_array(Helper.get_descenedents(self))
	
	for node in nodes:
		if node is Control:
			node.set_script(SCRIPT)
			node.connect("gui_input", node, "_on_gui_input")
