extends Resource
class_name FrameSceneCollection

export var database_view: PackedScene
export var database_entry: PackedScene
export var login_demo: PackedScene
export var email_view: PackedScene
export var email_entry: PackedScene
