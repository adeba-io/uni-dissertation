extends Node2D
class_name Website

export var _frame_scale := 1.0

onready var icon: TextureButton = $SiteIcon
onready var banner: Control = $PntBanner.get_child(0)

onready var frame_canvas: Node2D = $FrameCanvas
onready var _anim_player: AnimationPlayer = $AnimationPlayer
onready var frames: Array

var is_open := true

var representation: KGWebsite
var is_compliant := true

var violations := []

var _is_animating := false
var _d_frame_position := {}

func on_frames_received() -> void:
	for frame in frames:
		_d_frame_position[frame] = frame.position
		is_compliant = is_compliant and frame.is_compliant
		violations.append_array(frame.violations)
	
	violations = Helper.uniques_only(violations)
	print("%s is%s compliant" % [ name, "" if is_compliant else " not" ])
	print("\tViolations: " + str(GDPRViolations.get_violation_strings(violations)))
	($FrameStack as FrameStack).assign_frames(frames)

func set_active(value: bool) -> void:
	is_open = value
	frame_canvas.visible = value

func open() -> void:
	if not is_open:
		_animate(true, icon.rect_position)
		print(icon.rect_position)
		is_open = true

func close() -> void:
	if is_open:
		_animate(false, Vector2.ZERO)
		is_open = false

func _animate(to_open: bool, origin: Vector2) -> void:
	if not to_open:
		for frame in frames:
			_d_frame_position[frame] = frame.position
	
#	if to_open:
#		banner.visible = true
	
	_is_animating = true
	_anim_player.play("open" if to_open else "close")
	while _is_animating:
		banner.modulate.a = lerp(0.0, 1.0, _frame_scale)
		for frame in frames:
			frame.global_position = origin.linear_interpolate(_d_frame_position[frame], _frame_scale)
			frame.scale = Vector2.ONE * _frame_scale
		yield(get_tree(), "idle_frame")
	
#	if not to_open:
#		banner.visible = false

func _on_TextureButton_button_down() -> void:
	if is_open:
		close()
	else:
		open()

func _on_animation_finished(_anim_name: String) -> void:
	_is_animating = false
