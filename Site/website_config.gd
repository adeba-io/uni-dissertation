extends Node

export(Resource) var violation_chances
export(Resource) var frame_scenes
export var age_min := 16
export var age_max := 80

export var has_email := true
export(WebsiteParams.UseStatus) var has_phone_number := WebsiteParams.UseStatus.DONT_HAVE
export(WebsiteParams.UseStatus) var has_bank_card := WebsiteParams.UseStatus.DONT_HAVE
export(WebsiteParams.UseStatus) var has_real_name := WebsiteParams.UseStatus.DONT_HAVE

export var max_storage_years = 2

func _ready() -> void:
	var parent := get_parent()
	
	var params := WebsiteParams.new()
	params.name = parent.name
	params.violations = Helper.intersect_dict(RoundConfig.enabled_violations, violation_chances.violations)
	params.age_min = age_min
	params.age_max = age_max
	params.have_email = has_email
	params.have_phone_number = has_phone_number
	params.have_bank_card = has_bank_card
	params.have_real_name = has_real_name
	params.po_max_data_storage_years = max_storage_years
	
	var website := InfoGenerator.generate_data(RoundConfig.user_count, params)
	parent.representation = website
	
	yield(get_tree(), "idle_frame")
	WebsitePopulator.populate_website(parent, frame_scenes)
