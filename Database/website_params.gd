class_name WebsiteParams

enum UseStatus {
	NECESSARY,
	PROCESSED_TO_OBTAIN,
	NOT_AUTHORISED,
	DONT_HAVE
}

var name := ""

# violation { chance }
var violations := {}

var age_min := 16
var age_max := 80
var chance_of_having_parent := 0.74

var min_sign_up_date: Date = null
var min_last_use_date: Date = null

var blotched_identifications := false

var misc_email_chance := 0.3
var corruption_chance := 0.5
var proper_email_resolution_chance := 0.7
var email_types := []

# LoginDemo.Type
var got_consent: int = LoginDemo.Type.VALID
var show_login_demo := false
var have_email := true
var have_phone_number: int = UseStatus.DONT_HAVE
var have_bank_card: int = UseStatus.DONT_HAVE
var have_real_name: int = UseStatus.DONT_HAVE

var po_max_data_storage_years: int = 2
