class_name KGUser

var deleted := false
var withdrew_consent := false

var forename: String
var surname : String
var age: int

var username: String
var raw_password: String

var email_address: String
var phone_number: String
var bank_card: String

var sign_up_date: Date
var last_use_date: Date

var has_consented: bool

var email_ids := []

var parent := -1

# ####################################
# EXTRA DATA ZONE

var frequent_contacts := []
var history := []
var frequent_click_throughs := []
var search_history := []

# Maps a violation to an array of fields that violate it
# GDPRViolations -> [ DataType.e ]
var violations := {}

func add_violations(data_type: int, new_violations: Array) -> void:
	for v in new_violations:
		if v in violations:
			violations[v].append(data_type)
		else:
			violations[v] = [ data_type ]

func get_by_type(data_type: int):
	match data_type:
		DataTypes.e.FORENAME:
			return forename
		DataTypes.e.SURNAME:
			return surname
		DataTypes.e.AGE:
			return age
		DataTypes.e.USERNAME:
			return username
		DataTypes.e.PASSWORD:
			return raw_password
		DataTypes.e.SIGN_UP_DATE:
			return sign_up_date
		DataTypes.e.LAST_USE_DATE:
			return last_use_date
		DataTypes.e.EMAIL:
			return email_address
		DataTypes.e.PHONE_NUMBER:
			return phone_number
		DataTypes.e.BANK_CARD:
			return bank_card

func set_by_type(data_type: int, data):
	match data_type:
		DataTypes.e.FORENAME:
			forename = data
		DataTypes.e.SURNAME:
			surname = data
		DataTypes.e.USERNAME:
			username = data
		DataTypes.e.PASSWORD:
			raw_password = data
		DataTypes.e.SIGN_UP_DATE:
			sign_up_date = data
		DataTypes.e.LAST_USE_DATE:
			last_use_date = data
		DataTypes.e.EMAIL:
			email_address = data
		DataTypes.e.PHONE_NUMBER:
			phone_number = data
		DataTypes.e.BANK_CARD:
			bank_card = data

func get_violations_by_type(data_type: int) -> Array:
	var vios := []
	for violation in violations:
		if data_type in violations[violation]:
			vios.append(violation)
	return vios

func clear_data(data_type: int) -> void:
	match data_type:
		DataTypes.e.FORENAME:
			forename = ''
		DataTypes.e.SURNAME:
			surname = ''
		DataTypes.e.USERNAME:
			username = ''
		DataTypes.e.PASSWORD:
			raw_password = ''
		DataTypes.e.SIGN_UP_DATE:
			sign_up_date = null
		DataTypes.e.LAST_USE_DATE:
			last_use_date = null
		DataTypes.e.EMAIL:
			email_address = ''
		DataTypes.e.PHONE_NUMBER:
			phone_number = ''
		DataTypes.e.BANK_CARD:
			bank_card = ''
