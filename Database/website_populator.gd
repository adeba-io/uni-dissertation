class_name WebsitePopulator

const DBVIEW_FIELDS: Dictionary = {
	DataTypes.e.FORENAME: { data = 'Forename', width = 100, is_faulty = false, display_type = DBEntry.DisplayType.FADE },
	DataTypes.e.SURNAME: { data = 'Surname', width = 100, is_faulty = false, display_type = DBEntry.DisplayType.FADE },
	DataTypes.e.USERNAME: { data = 'Username', width = 130, is_faulty = false, display_type = DBEntry.DisplayType.FADE },
	DataTypes.e.PASSWORD: { data = 'Password', width = 100, is_faulty = false, display_type = DBEntry.DisplayType.FADE },
	DataTypes.e.AGE: { data = 'Age', width = 50, is_faulty = false, display_type = DBEntry.DisplayType.CUTOFF },
	DataTypes.e.EMAIL: { data = 'Email', width = 230, is_faulty = false, display_type = DBEntry.DisplayType.FADE },
	DataTypes.e.PHONE_NUMBER: { data = 'Phone Number', width = 200, is_faulty = false, display_type = DBEntry.DisplayType.CUTOFF },
	DataTypes.e.BANK_CARD: { data = 'Bank Card', width = 240, is_faulty = false, display_type = DBEntry.DisplayType.CUTOFF },
	DataTypes.e.SIGN_UP_DATE: { data = 'Sign Up', width = 130, is_faulty = false, display_type = DBEntry.DisplayType.CUTOFF },
	DataTypes.e.LAST_USE_DATE: { data = 'Last Up', width = 130, is_faulty = false, display_type = DBEntry.DisplayType.CUTOFF }
}

static func populate_website(website: Website, frame_scenes: FrameSceneCollection) -> void:
	var rep: KGWebsite = website.representation
	var frames := []
	
	# Create database views
	for view in rep.views:
		var dbview_data := [ Helper.get_by_keys(DBVIEW_FIELDS, view) ]
		var violations := []
		for user in rep.users:
			if user.deleted:
				continue
			
			violations.append_array(user.violations.keys())
			
			var entry := []
			for field_header in view:
				var field: Dictionary = DBVIEW_FIELDS[field_header].duplicate()
				field.data = str(user.get_by_type(field_header))
				field.is_faulty = user.get_violations_by_type(field_header).size() != 0
				entry.append(field)
			
			dbview_data.append(entry)
		
		var dbview: DBView = frame_scenes.database_view.instance()
		website.frame_canvas.add_child(dbview)
		dbview.setup(dbview_data, frame_scenes.database_entry, RoundConfig.show_faults)
		dbview.violations = Helper.uniques_only(violations)
		dbview.is_compliant = dbview.violations.size() == 0
		frames.append(dbview)
	
	# Parent View
	if rep.has_parents:
		var fields := [ DataTypes.e.USERNAME, DataTypes.e.USERNAME, DataTypes.e.AGE ]
		var headers := Helper.get_by_keys(DBVIEW_FIELDS, fields)
		headers[1].data = "Parent"
		headers[2].data = "Parent Age"
		headers[2].width = 140
		
		var violations := []
		var enum_value: int = GDPRViolations.e.PROPER_PARENTAL_CONSENT
		var dbview_data := [ headers ]
		for user in rep.users:
			if user.parent < 0:
				continue
			
			if enum_value in user.violations and not enum_value in violations:
				violations.append(enum_value)
			
			var entry := headers.duplicate(true)
			entry[0].data = user.username
			var parent: KGUser = rep.users[user.parent]
			entry[1].data = parent.username
			entry[2].data = str(parent.age)
			dbview_data.append(entry)
		
		var dbview: DBView = frame_scenes.database_view.instance()
		website.frame_canvas.add_child(dbview)
		dbview.setup(dbview_data, frame_scenes.database_entry, RoundConfig.show_faults)
		frames.append(dbview)
		dbview.violations = violations
		dbview.is_compliant = dbview.violations.size() == 0
	
	if rep.emails.size() != 0:
		var emails := {}
		var violations := []
		for email in rep.emails:
			if email.days_back in emails:
				emails[email.days_back].append(email)
			else:
				emails[email.days_back] = [ email ]
			
			if email.is_in_violation:
				violations.append(email.type)
		
		var email_view: EmailView = frame_scenes.email_view.instance()
		website.frame_canvas.add_child(email_view)
		email_view.assign_emails(rep, emails)
		frames.append(email_view)
		email_view.violations = Helper.uniques_only(violations)
		email_view.is_compliant = email_view.violations.size() == 0
	
	if rep.login_demo_type != -1:
		var login_demo: LoginDemo = frame_scenes.login_demo.instance()
		website.frame_canvas.add_child(login_demo)
		login_demo.setup(rep.login_demo_type)
		frames.append(login_demo)
		match rep.login_demo_type:
			LoginDemo.Type.NO_CONSENT, LoginDemo.Type.OBSCURE:
				login_demo.violations = [ GDPRViolations.e.CLEAR_REQUEST_FOR_CONSENT ]
				login_demo.is_compliant = false
	
	website.frames = frames
	website.on_frames_received()
