class_name Date

const BASELINE_YEAR := 2000

#const DAYS_IN_YEAR      := 365
#const DAYS_IN_LEAP_YEAR := 366

#var day: int
var month: int
var year: int

func _init():
	var date := OS.get_date()
#	day = date.day
	month = date.month
	year = date.year

func get_int_representation() -> int:
	var i := month
	for _y in range(BASELINE_YEAR, year):
		i += 12
	return i

func _to_string() -> String:
	return "%02d/%04d" % [ month, year ]
