class_name UDate

static func random_date(minimum: Date = null, mapping_function := FuncRef.new()) -> Date:
	var rng := RandomNumberGenerator.new()
	rng.randomize()
	
	var maximum := Date.new()
	if minimum == null:
		minimum = Date.new()
		minimum.year = Date.BASELINE_YEAR
		minimum.month = 1
	
	var r: float = mapping_function.call_func(rng.randf()) if mapping_function.is_valid() else rng.randf()
# warning-ignore:shadowed_variable
	var year: int = lerp(minimum.year, maximum.year, r)
	
	var m_min := minimum.month if year == minimum.year else 1
	var m_max := maximum.month if year == maximum.year else 12
# warning-ignore:shadowed_variable
	var month := rng.randi_range(m_min, m_max)
	
	var date := Date.new()
	date.year = year
	date.month = month
	return date

# warning-ignore:shadowed_variable
static func is_leap_year(year: int) -> bool:
	return year % 400 == 0 or (year % 100 != 0 and year % 4 == 0)

static func subtract(from: Date, months: int) -> Date:
	if months % 12 == 0:
		var years := months / 12
		var date := Date.new()
		date.month = from.month
		date.year = from.year - years
		return date
	
	var year := from.year - (months / 12)
	months = months % 12
	
	if months > from.month:
		months = 12 - (months - from.month)
		year -= 1
	else:
		months -= from.month
	
	var date := Date.new()
	date.month = months
	date.year = year
	return date
