extends Resource
class_name EmailOutline

export(Array, String, MULTILINE) var subjects: Array
export(Array, String, MULTILINE) var bodies: Array

func generate_email(params: Dictionary) -> Dictionary:
	var rng := RandomNumberGenerator.new()
	rng.randomize()
	return {
		subject = (subjects[rng.randi_range(0, subjects.size() - 1)].format(params)),
		body    = (bodies[rng.randi_range(0, bodies.size() - 1)].format(params))
	}
