class_name InfoGenerator

const FORENAMES := preload("res://Database/Names/forenames.tres").strings
const SURNAMES  := preload("res://Database/Names/surnames.tres").strings

const USERNAMES := preload('res://Database/UserData/usernames.tres').strings
const EMAIL_PROVIDERS := preload('res://Database/UserData/email_providers.tres').strings

const CARD_NUMBERS := preload('res://Database/UserData/card_numbers.tres').numbers

# #######################################
# EMAIL OUTLINES
# #######################################

#const EMAIL_TYPES := [
#	GDPRViolations.e.RIGHT_TO_WITHDRAW_CONSENT,
#	GDPRViolations.e.RIGHT_TO_CONFIRM_DATA_IS_BEING_PROCESSED,
#	GDPRViolations.e.RIGHT_TO_OBTAIN_RECTIFICATION,
#	GDPRViolations.e.RIGHT_TO_BE_FORGOTTEN
#]

const WITHDRAW_CONSENT := preload("res://Database/Emails/withdraw_consent.tres")
const CONFIRM := preload('res://Database/Emails/right_to_confirm.tres')
const CONFIRM_RES := preload('res://Database/Emails/right_to_confirm_response.tres')
const RECTIFY := preload('res://Database/Emails/rectification.tres')
const FORGET := preload('res://Database/Emails/right_to_forget.tres')

const RECTIFIABLE_DATA_TYPES := [
	DataTypes.e.FORENAME, DataTypes.e.SURNAME, DataTypes.e.AGE, DataTypes.e.PHONE_NUMBER, DataTypes.e.BANK_CARD
]

const MISC_EMAILS := [
	preload('res://Database/Emails/misc_company_party.tres')
]

# compliant_data { DataType => Dictionary of additional data }
# uncompliant_data { DataType => { violations [], chance float,  additional_data } }
static func generate_data(
	count: int, params: WebsiteParams
	) -> KGWebsite:
	var violations := []
	
	# Based on the violation chances alter some of the params
	params = _preprocess_params(params)
	
	var website := KGWebsite.new()
	website.personnel = _generate_personnel()
	website.login_demo_type = params.got_consent if params.show_login_demo else -1
	
	# Create the users
	website.users = []
	for _i in range(count):
		website.users.append(_create_user(params))
	
	if GDPRViolations.e.PROPER_PARENTAL_CONSENT in params.violations:
		website.has_parents = _fill_in_parents(website.users, params)
		
	# Generate emails
	website.emails = _generate_emails(website.users, website.personnel, params)
	
	# Alter things based on emails
	_resolve_emails(website.users, website.personnel, website.emails, params)
	
	# Determine where the violations are based on website policies and the user data
	_find_violations(website, params)
	
	# Determine what frames the site should have
	_determine_site_frames(website, params)
	
	return website

static func _preprocess_params(params: WebsiteParams) -> WebsiteParams:
	var rng := RandomNumberGenerator.new()
	rng.randomize()
	
	if GDPRViolations.e.SPECIFIC_EXPLICIT_LEGIT in params.violations:
		if params.have_bank_card == WebsiteParams.UseStatus.DONT_HAVE and rng.randf() < params.corruption_chance:
			params.have_bank_card = WebsiteParams.UseStatus.NOT_AUTHORISED
		
		if params.have_phone_number == WebsiteParams.UseStatus.DONT_HAVE and rng.randf() < params.corruption_chance:
			params.have_phone_number = WebsiteParams.UseStatus.NOT_AUTHORISED
		
		if params.have_real_name == WebsiteParams.UseStatus.DONT_HAVE and rng.randf() < params.corruption_chance:
			params.have_real_name = WebsiteParams.UseStatus.NOT_AUTHORISED
	
	if GDPRViolations.e.ENSURE_ACCURACY in params.violations:
		
		pass
	
	if GDPRViolations.e.IDENTIFICATION_OF_SUBJECTS in params.violations:
		params.blotched_identifications = rng.randi_range(1, 8) == 4
	
	if GDPRViolations.e.NOT_STORED_FOR_LONGER_THAN_NECESSARY in params.violations:
		var date := Date.new()
		date.year -= rng.randi_range(params.po_max_data_storage_years, 5)
		params.min_last_use_date = date
	else:
		var date := Date.new()
		date.year -= params.po_max_data_storage_years
		params.min_last_use_date = date
	
	if GDPRViolations.e.DEMONSTRATE_CONSENT in params.violations:
		params.got_consent = Helper.random_from_array([ LoginDemo.Type.OBSCURE, LoginDemo.Type.NO_CONSENT ], rng)
	if GDPRViolations.e.CLEAR_REQUEST_FOR_CONSENT in params.violations and rng.randf() < 0.3:
		params.got_consent = LoginDemo.Type.OBSCURE
	
	if GDPRViolations.e.RIGHT_TO_WITHDRAW_CONSENT in params.violations:
		print('right')
		params.email_types.append(GDPRViolations.e.RIGHT_TO_WITHDRAW_CONSENT)
	
	if GDPRViolations.e.PROPER_PARENTAL_CONSENT in params.violations:
		params.age_min = rng.randi_range(10, 20)
	
	if GDPRViolations.e.RIGHT_TO_CONFIRM_DATA_IS_BEING_PROCESSED in params.violations:
		params.email_types.append(GDPRViolations.e.RIGHT_TO_CONFIRM_DATA_IS_BEING_PROCESSED)
##	PURPOSES_CATEGORIES_RECIPIENTS_OF_PROCESSED_DATA,
#	LENGTH_OF_TIME_OF_DATA_STORAGE,
#	PROVIDE_COPY_OF_DATA_BEING_PROCESSED,
	if GDPRViolations.e.RIGHT_TO_OBTAIN_RECTIFICATION in params.violations:
		params.email_types.append(GDPRViolations.e.RIGHT_TO_OBTAIN_RECTIFICATION)
		
	if GDPRViolations.e.RIGHT_TO_BE_FORGOTTEN in params.violations:
		params.email_types.append(GDPRViolations.e.RIGHT_TO_BE_FORGOTTEN)
#	DATA_BREACH_INFORM_SUPERVISORY_BODY,
#	DATA_BREACH_INFORM_SUBJECTS
	
	return params

# compliant_data { DataType => Dictionary of additional data }
# uncompliant_data { DataType => { violations [], chance float,  additional_data } }
static func _create_user(params: WebsiteParams) -> KGUser:
	var user := KGUser.new()
	var rng := RandomNumberGenerator.new()
	rng.randomize()

	if params.blotched_identifications:
		user.username = "user-%05d" % rng.randi_range(0, 99999)
		user.age = 50
		if params.have_real_name != WebsiteParams.UseStatus.DONT_HAVE:
			user.forename = '-----'
			user.surname = '-----'
	else:
		user.username = USERNAMES[rng.randi_range(0, USERNAMES.size() - 1)]
		user.age = rng.randi_range(params.age_min, params.age_max)
		if params.have_real_name != WebsiteParams.UseStatus.DONT_HAVE:
			user.forename = FORENAMES[rng.randi_range(0, FORENAMES.size() - 1)]
			user.surname = SURNAMES[rng.randi_range(0, SURNAMES.size() - 1)]
	
	user.sign_up_date = UDate.random_date(params.min_sign_up_date)
	user.last_use_date = UDate.random_date(params.min_last_use_date if params.min_last_use_date else user.sign_up_date)
	
	if params.have_email:
		if params.blotched_identifications:
			user.email_address = user.username + '@' + params.name + '.com'
		else:
			user.email_address = generate_email_address(user.username, rng)
	
	if params.have_phone_number != WebsiteParams.UseStatus.DONT_HAVE:
		user.phone_number = generate_phone_number(rng)
	
	if params.have_bank_card != WebsiteParams.UseStatus.DONT_HAVE:
		user.bank_card = generate_bank_card(rng)
	
	user.has_consented = params.got_consent != LoginDemo.Type.NO_CONSENT
	
	return user

static func _fill_in_parents(users: Array, params: WebsiteParams) -> bool:
	var underage_indexes := []
	var legal_indexes := []
	var rng := RandomNumberGenerator.new()
	rng.randomize()
	for i in range(users.size()):
		if users[i].age < 16:
			underage_indexes.append(i)
		else:
			legal_indexes.append(i)
			users[i].has_consented = true
	
	print(underage_indexes)
	print(legal_indexes)
	var has_parents := false
	for i in underage_indexes:
		if rng.randf() < params.chance_of_having_parent:
			var y := rng.randi_range(0, legal_indexes.size() - 1)
			users[i].parent = y
			has_parents = true
		else:
			users[i].parent = -2 # To make sure it has been recognised
	return has_parents

static func _generate_personnel() -> Array:
	var rng := RandomNumberGenerator.new()
	rng.randomize()
	var personnel := []
	
	var count := rng.randi_range(5, 8)
	for _i in range(count):
		var f := rng.randi_range(0, FORENAMES.size() - 1)
		var s := rng.randi_range(0, SURNAMES.size() - 1)	
		personnel.append(KGPersonnel.new(FORENAMES[f], SURNAMES[s]))
	
	return personnel

static func _get_email_vars(user: KGUser, staff: KGPersonnel, params: WebsiteParams) -> Dictionary:
	return {
		site_name = params.name,
		personnel_name = staff.forename,
		personnel_fullname = staff.forename + " " + staff.surname,
		username = user.username,
		user_sign_up = str(user.sign_up_date)
	}

static func _generate_emails(users: Array, personnel: Array, params: WebsiteParams) -> Array:
	if not params.have_email:
		return []
	
	var rng := RandomNumberGenerator.new()
	rng.randomize()
	var idx_adult := []
	for i in range(users.size()):
		if users[i].age >= 16:
			idx_adult.append(i)
	
	var emails := []
	var count := rng.randi_range(idx_adult.size() / 2, idx_adult.size() * 2)
	
	var misc_email_chance := params.misc_email_chance
	if params.email_types.size() == 0:
		misc_email_chance = 1.1
	
	print(misc_email_chance)
	for _a in range(count):
		var i_user: int = idx_adult[rng.randi_range(0, idx_adult.size() - 1)]
		var i_personnel: int = rng.randi_range(0, personnel.size() - 1)
		
		var user: KGUser = users[i_user]
		var staff: KGPersonnel = personnel[i_personnel]
		
		var vars := _get_email_vars(user, staff, params)
		var additional := {}
		
		var type := -1
		var content: Dictionary
		
		if rng.randf() < misc_email_chance:
			content = MISC_EMAILS[rng.randi_range(0, MISC_EMAILS.size() - 1)].generate_email(vars)
		else:
			print('other')
			type = params.email_types[rng.randi_range(0, params.email_types.size() - 1)]
			match type:
				GDPRViolations.e.RIGHT_TO_WITHDRAW_CONSENT:
					user.withdrew_consent = true
					content = WITHDRAW_CONSENT.generate_email(vars)
				GDPRViolations.e.RIGHT_TO_OBTAIN_RECTIFICATION:
					var field_count := rng.randi_range(1, 3)
					
					vars.fields_data = []
					vars.fields = ''
					vars.corrections_data = []
					vars.corrections = ''
					
					var fields := RECTIFIABLE_DATA_TYPES.duplicate()
					for _i in range(field_count):
						var field: int = fields[rng.randi_range(0, fields.size() - 1)]
						vars.fields_data.append(field)
						fields.erase(field)
						
						var cor
						match field:
							DataTypes.e.FORENAME:
								var names := FORENAMES.duplicate()
								names.erase(user.forename)
								cor = names[rng.randi_range(0, names.size() - 1)]
							DataTypes.e.SURNAME:
								var names := FORENAMES.duplicate()
								names.erase(user.forename)
								cor = names[rng.randi_range(0, names.size() - 1)]
							DataTypes.e.AGE:
								cor = max(user.age + rng.randi_range(-4, 4), 16)
							DataTypes.e.PHONE_NUMBER:
								cor = generate_phone_number(rng)
							DataTypes.e.BANK_CARD:
								cor = generate_bank_card(rng)
						
						vars.corrections_data.append(cor)
						
					for i in range(field_count):
						if i == field_count - 1 and i != 0:
							vars.fields += ' and '
							vars.corrections += ' and '
						elif i != 0:
							vars.fields += ', '
							vars.corrections += ', '
						
						vars.fields += DataTypes.get_name(vars.fields_data[i])
						vars.corrections += str(vars.corrections_data[i])
					
					content = RECTIFY.generate_email(vars)
					additional.fields = vars.fields_data
					additional.corrections = vars.corrections_data
				GDPRViolations.e.RIGHT_TO_CONFIRM_DATA_IS_BEING_PROCESSED:
					content = CONFIRM.generate_email(vars)
				GDPRViolations.e.RIGHT_TO_BE_FORGOTTEN:
					content = FORGET.generate_email(vars)
		
		var email := KGEmail.new(content)
		email.type = type
		email.from = i_user
		email.to = i_personnel
		email.days_back = rng.randi_range(0, 7)
		email.additional_data = additional
		emails.append(email)
	return emails

static func _resolve_emails(users: Array, personnel: Array, emails: Array, params: WebsiteParams) -> void:
	var rng := RandomNumberGenerator.new()
	rng.randomize()
	var chance: float = params.proper_email_resolution_chance
	for email in emails:
		if email.type < 0:
			continue
		var user: KGUser = users[email.from]
		
		match email.type:
			GDPRViolations.e.RIGHT_TO_WITHDRAW_CONSENT:
				var to_remove := []
				if rng.randf() < chance and params.have_bank_card == WebsiteParams.UseStatus.PROCESSED_TO_OBTAIN:
					to_remove.append(DataTypes.e.BANK_CARD)
				if rng.randf() < chance and params.have_phone_number == WebsiteParams.UseStatus.PROCESSED_TO_OBTAIN:
					to_remove.append(DataTypes.e.PHONE_NUMBER)
				if rng.randf() < chance and params.have_real_name == WebsiteParams.UseStatus.PROCESSED_TO_OBTAIN:
					to_remove.append(DataTypes.e.FORENAME)
					to_remove.append(DataTypes.e.SURNAME)
				
				for data_type in to_remove:
					user.clear_data(data_type)
			
			GDPRViolations.e.RIGHT_TO_OBTAIN_RECTIFICATION:
				var fields: Array = email.additional_data.fields
				var corrections: Array = email.additional_data.corrections
				
				for i in range(fields.size()):
					if rng.randf() < chance:
						user.set_by_type(fields[i], corrections[i])
			
			GDPRViolations.e.RIGHT_TO_BE_FORGOTTEN:
				if rng.randf() < chance:
					user.deleted = true
			
			GDPRViolations.e.RIGHT_TO_CONFIRM_DATA_IS_BEING_PROCESSED:
				if rng.randf() < chance:
					var staff: KGPersonnel = personnel[email.to]
					var vars := _get_email_vars(user, staff, params)
					vars.prev_subject = email.subject
					vars.processed_data = ''
					if params.have_phone_number == WebsiteParams.UseStatus.PROCESSED_TO_OBTAIN:
						vars.processed_data = 'Your phone number\n'
					if params.have_bank_card == WebsiteParams.UseStatus.PROCESSED_TO_OBTAIN:
						vars.processed_date += 'Your bank card\n'
					if params.have_real_name == WebsiteParams.UseStatus.PROCESSED_TO_OBTAIN:
						vars.processed_date += 'Your real name and surname\n'
					
					var content := CONFIRM_RES.generate_email(vars)
					var response := KGEmail.new(content)
					response.from = -(email.to + 1)
					response.to = -(email.from + 1)
					response.days_back = rng.randi_range(0, email.days_back)
					email.response_id = emails.size()
					emails.append(response)

static func _find_violations(website: KGWebsite, params: WebsiteParams) -> void:
	
	if GDPRViolations.e.SPECIFIC_EXPLICIT_LEGIT in params.violations:
		var vio_fields := []
		if params.have_bank_card == WebsiteParams.UseStatus.NOT_AUTHORISED:
			vio_fields.append(DataTypes.e.BANK_CARD)
		if params.have_phone_number == WebsiteParams.UseStatus.NOT_AUTHORISED:
			vio_fields.append(DataTypes.e.PHONE_NUMBER)
		if params.have_real_name == WebsiteParams.UseStatus.NOT_AUTHORISED:
			vio_fields.append(DataTypes.e.FORENAME)
			vio_fields.append(DataTypes.e.SURNAME)
		
		if vio_fields.size() != 0:
			_fill_in_user_violations(website.users, GDPRViolations.e.SPECIFIC_EXPLICIT_LEGIT, vio_fields)
	
	if GDPRViolations.e.ENSURE_ACCURACY in params.violations:
		pass
	
	if GDPRViolations.e.IDENTIFICATION_OF_SUBJECTS in params.violations:
		if params.blotched_identifications:
			_fill_in_user_violations(
				website.users,
				GDPRViolations.e.IDENTIFICATION_OF_SUBJECTS,
				[ DataTypes.e.USERNAME, DataTypes.e.AGE, DataTypes.e.FORENAME, DataTypes.e.SURNAME ]
			)
	
	if GDPRViolations.e.NOT_STORED_FOR_LONGER_THAN_NECESSARY in params.violations:
		var violating_users := []
		var today := Date.new()
		for user in website.users:
			var diff: int = today.get_int_representation() - user.last_use_date.get_int_representation()
			if diff > (params.po_max_data_storage_years * 12):
				violating_users.append(user)
		
		_fill_in_user_violations(
			violating_users,
			GDPRViolations.e.NOT_STORED_FOR_LONGER_THAN_NECESSARY, [ DataTypes.e.LAST_USE_DATE ])
	
	if GDPRViolations.e.DEMONSTRATE_CONSENT in params.violations:
		if params.got_consent == LoginDemo.Type.NO_CONSENT:
			_fill_in_user_violations(
				website.users,
				GDPRViolations.e.DEMONSTRATE_CONSENT,
				[ DataTypes.e.HAS_CONSENTED ]
			)
	
	if GDPRViolations.e.RIGHT_TO_WITHDRAW_CONSENT in params.violations:
		for i in range(website.users.size()):
			var user: KGUser = website.users[i]
			if not user.withdrew_consent:
				continue
			
			var violating_data_types := []
			if params.have_phone_number == WebsiteParams.UseStatus.PROCESSED_TO_OBTAIN and not user.phone_number.empty():
				violating_data_types.append(DataTypes.e.PHONE_NUMBER)
			if params.have_bank_card == WebsiteParams.UseStatus.PROCESSED_TO_OBTAIN and not user.bank_card.empty():
				violating_data_types.append(DataTypes.e.BANK_CARD)
			if params.have_real_name == WebsiteParams.UseStatus.PROCESSED_TO_OBTAIN:
				if not user.forename.empty():
					violating_data_types.append(DataTypes.e.FORENAME)
				if not user.surname.empty():
					violating_data_types.append(DataTypes.e.SURNAME)
			
			_fill_in_user_violations(
				[ user ], GDPRViolations.e.RIGHT_TO_WITHDRAW_CONSENT,
				violating_data_types
			)
			
			for email in website.emails:
				if not (email.from == i or email.type == GDPRViolations.e.RIGHT_TO_WITHDRAW_CONSENT):
					continue
				
				email.is_in_violation = true
	
	if GDPRViolations.e.CLEAR_REQUEST_FOR_CONSENT in params.violations:
		if params.got_consent == LoginDemo.Type.NO_CONSENT:
			_fill_in_user_violations(
				website.users,
				GDPRViolations.e.CLEAR_REQUEST_FOR_CONSENT,
				[ DataTypes.e.HAS_CONSENTED ]
			)
	
	if GDPRViolations.e.PROPER_PARENTAL_CONSENT in params.violations:
		var violating_users := []
		for user in website.users:
			if user.age < 16 and user.parent == -1:
				violating_users.append(user)
		
		_fill_in_user_violations(violating_users, GDPRViolations.e.PROPER_PARENTAL_CONSENT, [ DataTypes.e.AGE ])
	
	if GDPRViolations.e.RIGHT_TO_CONFIRM_DATA_IS_BEING_PROCESSED in params.violations:
		for email in website.emails:
			email.is_in_violation = email.is_in_violation or (
				email.type == GDPRViolations.e.RIGHT_TO_CONFIRM_DATA_IS_BEING_PROCESSED
				and email.days_back > 1
				and email.response_id == -1
			)
##	PURPOSES_CATEGORIES_RECIPIENTS_OF_PROCESSED_DATA,
#	if GDPRViolations.e.LENGTH_OF_TIME_OF_DATA_STORAGE in params.violations:
#	PROVIDE_COPY_OF_DATA_BEING_PROCESSED,
	if GDPRViolations.e.RIGHT_TO_OBTAIN_RECTIFICATION in params.violations:
		for email in website.emails:
			if email.type != GDPRViolations.e.RIGHT_TO_OBTAIN_RECTIFICATION or email.days_back < 2:
				continue
			
			var violating_fields := []
			var user: KGUser = website.users[email.from]
			var fields: Array = email.additional_data.fields
			var corrections: Array = email.additional_data.corrections
			
			for i in range(fields.size()):
				var data = user.get_by_type(fields[i])
				var same := false
				if data is String:
					same = fields[i].casecmp(corrections[i])
				else:
					same = fields[i] == corrections[i]
				if not same:
					violating_fields.append(fields[i])
			
			if violating_fields.size() != 0:
				_fill_in_user_violations([ user ], GDPRViolations.e.RIGHT_TO_OBTAIN_RECTIFICATION, violating_fields)
				email.is_in_violation = true
	
	if GDPRViolations.e.RIGHT_TO_BE_FORGOTTEN in params.violations:
		var violating_users := []
		for email in website.emails:
			if email.type == GDPRViolations.e.RIGHT_TO_BE_FORGOTTEN and email.days_back > 1:
				continue
			
			var user: KGUser = website.users[email.from]
			if user.deleted:
				continue
			
			violating_users.append(user)
		
		_fill_in_user_violations(violating_users, GDPRViolations.e.RIGHT_TO_BE_FORGOTTEN, DataTypes.e.values())
#	DATA_BREACH_INFORM_SUPERVISORY_BODY,
#	DATA_BREACH_INFORM_SUBJECTS

static func _determine_site_frames(website: KGWebsite, params: WebsiteParams) -> void:
	var views := []
	var all_types := DataTypes.e.values()
	var rng := RandomNumberGenerator.new()
	
	var view := [ DataTypes.e.USERNAME ]
	if GDPRViolations.e.PROPER_PARENTAL_CONSENT in params.violations:
		view.append(DataTypes.e.AGE)
	
	if params.have_email:
		view.append(DataTypes.e.EMAIL)
		all_types.erase(DataTypes.e.EMAIL)
	
	if view.size() == 3 and rng.randf() < 0.5:
		views.append(view)
		view = [ DataTypes.e.USERNAME ]
	
	if params.have_real_name != WebsiteParams.UseStatus.DONT_HAVE:
		view.append_array([ DataTypes.e.FORENAME, DataTypes.e.SURNAME ])
	
	if view.size() > 2:
		views.append(view)
		view = [ DataTypes.e.USERNAME ]
	
	if params.have_bank_card != WebsiteParams.UseStatus.DONT_HAVE:
		view.append(DataTypes.e.BANK_CARD)
	
	if params.have_phone_number != WebsiteParams.UseStatus.DONT_HAVE:
		view.append(DataTypes.e.PHONE_NUMBER)
	
	if view.size() > 2:
		views.append(view)
		view = [ DataTypes.e.USERNAME ]
	
	view.append(DataTypes.e.LAST_USE_DATE)
	views.append(view)
	
	website.views = views

static func _fill_in_user_violations(users: Array, violation: int, fields: Array) -> void:
	for user in users:
		if violation in user.violations:
			user.violations[violation].append_array(fields)
		else:
			user.violations[violation] = fields

static func generate_email_address(name: String, rng: RandomNumberGenerator) -> String:
	var email_prefix: String
	if not name.empty():
		email_prefix = name
	else:
		email_prefix = USERNAMES[rng.randi_range(0, USERNAMES.size() - 1)]
	
	var email_suffix: String = '@' + EMAIL_PROVIDERS[rng.randi_range(0, EMAIL_PROVIDERS.size() - 1)]
	
	match rng.randi_range(-2, 3):
		-2:
			pass
		-1:
			pass
		0:
			email_prefix = email_prefix.to_lower()
		1:
			email_prefix = email_prefix.to_upper()
		2:
			email_prefix = str(rng.randi_range(100_000_000, 999_999_999))
		3:
			email_prefix += str(rng.randi_range(0, 999))
	
	return email_prefix + email_suffix

static func generate_phone_number(rng: RandomNumberGenerator) -> String:
	return '0789%d %06d' % [ rng.randi_range(0, 9), rng.randi_range(0, 999_999) ]

static func generate_bank_card(rng: RandomNumberGenerator) -> String:
	return "%1d%03d %04d %04d %04d" % [ 
		Helper.random_from_array(CARD_NUMBERS, rng),
		rng.randi_range(0, 999), rng.randi_range(0, 9999),
		rng.randi_range(0, 9999), rng.randi_range(0, 9999) ]
