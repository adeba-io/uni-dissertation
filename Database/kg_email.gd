class_name KGEmail

# RIGHT_TO_WITHDRAW_CONSENT,
# RIGHT_TO_CONFIRM_DATA_IS_BEING_PROCESSED,
# LENGTH_OF_TIME_OF_DATA_STORAGE,
# PROVIDE_COPY_OF_DATA_BEING_PROCESSED,
# RIGHT_TO_OBTAIN_RECTIFICATION,
# RIGHT_TO_BE_FORGOTTEN,
# DATA_BREACH_INFORM_SUPERVISORY_BODY,
# DATA_BREACH_INFORM_SUBJECTS
var type := -1
var is_in_violation := false
var subject: String
var body: String
var days_back: int

var from: int
var to: int

var additional_data := {}

var response_id := -1

func _init(content: Dictionary) -> void:
	subject = content.subject
	body = content.body
