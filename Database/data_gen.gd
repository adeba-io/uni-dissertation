class_name DataGen

const USERNAMES := preload('res://Database/UserData/usernames.tres').strings

static func gen_username(email: String, from_email_chance := 0.2) -> String:
	var rng := RandomNumberGenerator.new()
	rng.randomize()
	if email.empty() or rng.randf() > from_email_chance:
		return USERNAMES[rng.randi_range(0, USERNAMES.size() - 1)]
	
	return email.split('@', true, 1)[0]
