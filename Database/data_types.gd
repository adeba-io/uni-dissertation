class_name DataTypes

enum e {
	FORENAME,
	SURNAME,
	USERNAME,
	PASSWORD, 
	AGE,
	EMAIL,
	PHONE_NUMBER,
	BANK_CARD,
	SIGN_UP_DATE,
	LAST_USE_DATE,
	HAS_CONSENTED,
	
	FREQUENT_CONTACTS
}

static func get_name(type: int) -> String:
	match type:
		e.FORENAME:
			return "forename"
		e.SURNAME:
			return "surname"
		e.USERNAME:
			return "username"
		e.PASSWORD:
			return "password"
		e.AGE:
			return "age"
		e.EMAIL:
			return "email"
		e.PHONE_NUMBER:
			return "phone number"
		e.BANK_CARD:
			return "bank card"
		e.SIGN_UP_DATE:
			return "sign up date"
		e.LAST_USE_DATE:
			return "last use date"
	return 'forename'
