extends Resource
class_name LevelSettings

export var time := 120
export var user_count := 5

export(Array, GDPRViolations.e) var violations := []
