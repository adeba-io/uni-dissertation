extends Control

export(PackedScene) var game_scene: PackedScene

onready var anim_player: AnimationPlayer = $AnimationPlayer

func _on_BtnPlay_button_down():
	if Game.free_mode_unlocked:
		anim_player.play("to_config")
	else:
		Game.next_level()

func _on_Quit_button_down():
	get_tree().quit()

func _on_BtnBack_button_down():
	anim_player.play("to_main")


func _on_freemode_button_down():
	Game.unlock_free_mode()
