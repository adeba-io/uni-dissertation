extends Spatial

export(NodePath) var _sun_path: NodePath

onready var _earth: Material = ($Albedo as MeshInstance).get_active_material(0)

var _sun: Spatial

func _ready() -> void:
	_sun = get_node(_sun_path)

func _process(_delta: float) -> void:
	_earth.set_shader_param("lightPos", _sun.global_transform.origin)
