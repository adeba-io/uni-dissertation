extends Spatial

export var by_date_time := true
export var rotation_speed := 0.1

var _midday_pos: float

const MAX_TIME := 86400.0

func _ready():
	_midday_pos = transform.basis.get_euler().y

func _process(delta: float) -> void:
	if by_date_time:
		var date_time: Dictionary = OS.get_datetime(true)
		var time: float =  date_time['second'] + (date_time['minute'] * 60.0) + (date_time['hour'] * 3600) - MAX_TIME / 2
		var rot: float = (time / MAX_TIME) * 2 * PI
		rot += _midday_pos
		
		transform.basis = Basis()
		rotate_y(rot)
	else:
		rotate_y(rotation_speed * delta)
