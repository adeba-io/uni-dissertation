extends HSplitContainer

export var _default_on := true
export(GDPRViolations.e) var violation_type: int

func upload_config_data() -> void:
	if RoundConfig.enabled_violations.has(violation_type):
		print("Duplicate violation flag")
	elif $CheckButton.pressed:
		RoundConfig.enabled_violations.append(violation_type)

func _ready() -> void:
	$CheckButton.pressed = _default_on
	$Label.text = GDPRViolations.get_full_string(violation_type)
