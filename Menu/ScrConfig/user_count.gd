extends VSlider

export(NodePath) var _path_lbl_site_count: NodePath

onready var _lbl_site_count: Label = get_node(_path_lbl_site_count)

func _ready() -> void:
	_lbl_site_count.text = str(int(value))

func _on_self_value_changed(value: float) -> void:
	_lbl_site_count.text = str(int(value))

func upload_config_data() -> void:
	RoundConfig.user_count = int(value)
