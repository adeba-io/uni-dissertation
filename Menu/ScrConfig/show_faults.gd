extends Button

export var txt_show_faults := "Show Faults"
export var txt_hide_faults := "Hide Faults"

func upload_config_data() -> void:
	RoundConfig.show_faults = pressed

func _on_self_toggled(button_pressed: bool) -> void:
	text = txt_hide_faults if button_pressed else txt_show_faults
