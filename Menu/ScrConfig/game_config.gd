extends Control

const UPLOAD_FUNCTION := 'upload_config_data'

func _on_BtnStart_button_down() -> void:
	for cont in Helper.get_descendents_in_group(self, [ 'config' ]):
		if cont.has_method(UPLOAD_FUNCTION):
			cont.call(UPLOAD_FUNCTION)
	
	# Signal for the round to start
	MngGameplay.begin_round('Custom')
