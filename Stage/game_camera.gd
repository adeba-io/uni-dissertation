extends Camera2D

export var _min_zoom := 0.5
export var _max_zoom := 2.0
export var _zoom_speed := 0.1
export var _pan_speed := 2.0

onready var _zoom_level := zoom.x

var _is_panning := false

func _unhandled_input(event: InputEvent) -> void:
	var zoom_level := float(event.is_action_pressed("zoom_out")) - float(event.is_action_pressed('zoom_in'))
	_zoom_level = clamp(_zoom_level + (zoom_level * _zoom_speed), _min_zoom, _max_zoom)
	zoom = Vector2(_zoom_level, _zoom_level)
	
	if event.is_action_pressed("pan"):
		_is_panning = true
		
	if event.is_action_released("pan"):
		_is_panning = false
	
	if _is_panning and event is InputEventMouseMotion:
		offset -= event.relative * _pan_speed

