extends Label
class_name Countdown

onready var _timer: Timer = $Timer

var _is_counting_down := false

func start_countdown(duration: int) -> void:
	_timer.start(duration)
	_is_counting_down = true

func stop() -> void:
	_timer.stop()
	_is_counting_down = false

func connect_to_timeout(obj, func_name: String) -> void:
	var err := _timer.connect("timeout", obj, func_name)
	if err != OK:
		print(err)

func _process(_delta: float) -> void:
	if not _is_counting_down:
		return
	
	var time_left := int(_timer.time_left)
# warning-ignore:integer_division
	var mins := time_left / 60
	var secs := time_left % 60
	text = "%02d : %02d" % [ mins, secs ]


func _on_timeout():
	_is_counting_down = false
