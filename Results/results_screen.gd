extends Control
class_name ResultsScreen

export(NodePath) var _p_successes: NodePath
export(NodePath) var _p_misses: NodePath
export(NodePath) var _p_final_score: NodePath

onready var _lbl_successes: Label = get_node(_p_successes)
onready var _lbl_misses: Label = get_node(_p_misses)
onready var _lbl_final_score: Label = get_node(_p_final_score)
onready var _anim_player: AnimationPlayer = $AnimationPlayer

func _ready() -> void:
	visible = false

func calculate_final_score(scores: Dictionary) -> int:
	var total: float = scores['successes'] + scores['misses']
	var ratio: float = scores['successes'] / total if total != 0 else 0
	return int(100 * ratio)

func start(scores: Dictionary) -> void:
	_lbl_successes.text = str(scores['successes'])
	_lbl_misses.text = str(scores['misses'])
	_lbl_final_score.text = str(calculate_final_score(scores))
	
	_anim_player.play("awake")


func _on_Next_button_down():
	Game.next_level()
